Toda una generación de internautas ha crecido con los torrents y todas las ventajas que pueden ofrecer. Aunque los torrents han desempeñado un papel fundamental en la historia del intercambio en línea, es posible que pronto pasen a la historia, dando paso a alternativas como los enlaces magnéticos.

Si has estado navegando por sitios de torrents populares como The Pirate Bay durante un tiempo, probablemente habrás visto un enlace con un icono magnético junto al archivo que quieres descargar. Se trata de un enlace magnético, una de las formas cada vez más populares de descargar medios en línea.

Ver el índice 
¿Qué son los enlaces magnéticos?
Un enlace magnet es un hipervínculo que le da acceso directo a un código hash de un archivo torrent. Un código hash es un término técnico para un valor numérico que identifica de forma única un paquete. Cuando se accede a un sitio web a través de su URL, se está pidiendo a un servidor remoto que haga un hash de la solicitud para que pueda entregar el sitio web deseado.

Asimismo, se puede crear inmediatamente una conexión magnet con cualquier cliente torrent para encontrar pares válidos que compartan el mismo archivo. Utiliza las DHT, también conocidas como tablas de hash distribuidas, para encontrar de forma transparente la dirección IP de los compañeros que comparten el mismo archivo.

El torrente no rastreable suele utilizarse indistintamente con el término enlace magnético, aunque el primero es una categoría amplia que incluye una gran variedad de tecnologías distribuidas.

¿Cómo funciona un enlace magnético?
Veamos brevemente los detalles de cómo funcionan estas pequeñas piezas de información en el mundo real.

Los enlaces magnéticos, o URIs magnéticos (Uniform Resource Identifiers), consisten en varios argumentos compactos formateados como cadenas de preguntas y respuestas. Veamos el archivo "about" de un enlace magnético común:

La palabra clave "imán:" se da con uno o más parámetros, al igual que las funciones algebraicas denotan las variables con letras. El argumento más importante es el argumento "xt", que se refiere al "tema exacto" que se pasará al cliente torrent para buscar el medio deseado. En el ejemplo anterior, buscamos un archivo de descarga que está dividido en dos partes, xt.1 y xt.2.

El signo de interrogación después de "imán:" significa que estamos pidiendo información dividida en preguntas o valores de pregunta y respuesta. Cada parámetro va seguido de su valor explícito con un signo igual, mientras que los argumentos adicionales se separan con el símbolo "&".

¿Cómo abrir un enlace magnético?
Por extraño que parezca, es probable que haya utilizado enlaces magnéticos sin darse cuenta.

Un enlace magnético se abre sin esfuerzo y funciona igual en cualquier navegador. En primer lugar, visite un sitio de torrents que ofrezca enlaces magnéticos como opción de descarga, como [Dontorrents](https://ibingz.com/dontorrent-alternativas/). Basta con hacer clic en el enlace con el símbolo del imán, como en la imagen siguiente.





